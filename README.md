# SF-Visio-Tarefa GDLB

## Name
Monitorig Third Party Application with Grafana, Prometheus and Node-Exporter, orchestrated with Docker Compose (Unfinished).

## Description
The goal was to make a CD/CI project running Prometheus, Grafana, Node-Exporter; all via Docker Compose to automate monitoring of the metrics of a specific designed application, also running via docker compose. Since it hasn't been finished, it won't fulfil its purpose, for the moment. 


## License
This code is free to use, although it is still a work in progress.

## Project status
I've run out of time to complete this challenge for the time being, but I hope to come back in the future to complete it. 
